const express = require('express');
const app = express();
const os = require("os");
const hostname = os.hostname();
const MongoClient = require('mongodb').MongoClient;
const assert = require('assert');
// Connection URL
const dbUrl = 'mongodb://db:27017';
// Database Name
const dbName = 'myproject';

// Use connect method to connect to the server
//  MongoClient.connect(dbUrl, function(err, client) {
//     res.send("Check");
//   assert.equal(null, err);
//   console.log("Connected successfully to server");

//   const db = client.db(dbName);

//   client.close();      
// });

app.get('/', (req, res) => res.send('Hello World, from '+ hostname + '!'));

app.get('/db-check', (req, res) => {
    
    res.send("End!!!");
});

app.listen(80, () => console.log('Example app listening on port 80!'));
