.SILENT:
.PHONY: help

# Copied from https://gist.github.com/rcmachado/af3db315e31383502660#file-makefile

help:
	printf "Available targets\n\n"
	awk '/^[a-zA-Z\-\_0-9]+:/ { \
		helpMessage = match(lastLine, /^## (.*)/); \
		if (helpMessage) { \
			helpCommand = substr($$1, 0, index($$1, ":")-1); \
			helpMessage = substr(lastLine, RSTART + 3, RLENGTH); \
			printf "%-15s %s\n", helpCommand, helpMessage; \
		} \
	} \
	{ lastLine = $$0 }' $(MAKEFILE_LIST)

## Boot up stack and start lodabalancer and applications
up: stack-up stack-start	

## Boot up the stack
stack-up:
	docker-compose up -d

## Kickstart Loadbalancer and applications
stack-start:
	docker exec -it ansible_playground_control ansible-playbook playbooks/stack-kick.yml
